import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioS5Component } from './portfolio-s5.component';

describe('PortfolioS5Component', () => {
  let component: PortfolioS5Component;
  let fixture: ComponentFixture<PortfolioS5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortfolioS5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioS5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PortfolioRoutingModule } from './portfolio-routing.module';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { PortfolioS1Component } from './portfolio-s1/portfolio-s1.component';
import { PortfolioS2Component } from './portfolio-s2/portfolio-s2.component';
import { PortfolioS3Component } from './portfolio-s3/portfolio-s3.component';
import { PortfolioS4Component } from './portfolio-s4/portfolio-s4.component';
import { PortfolioS5Component } from './portfolio-s5/portfolio-s5.component';


@NgModule({
  declarations: [
    PortfolioComponent,
    PortfolioS1Component,
    PortfolioS2Component,
    PortfolioS3Component,
    PortfolioS4Component,
    PortfolioS5Component
  ],
  imports: [
    CommonModule,
    PortfolioRoutingModule
  ]
})
export class PortfolioModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioS1Component } from './portfolio-s1.component';

describe('PortfolioS1Component', () => {
  let component: PortfolioS1Component;
  let fixture: ComponentFixture<PortfolioS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortfolioS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

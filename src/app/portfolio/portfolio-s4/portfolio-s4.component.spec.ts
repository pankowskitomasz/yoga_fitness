import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioS4Component } from './portfolio-s4.component';

describe('PortfolioS4Component', () => {
  let component: PortfolioS4Component;
  let fixture: ComponentFixture<PortfolioS4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortfolioS4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioS4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioS2Component } from './portfolio-s2.component';

describe('PortfolioS2Component', () => {
  let component: PortfolioS2Component;
  let fixture: ComponentFixture<PortfolioS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortfolioS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

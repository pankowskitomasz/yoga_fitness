import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioS3Component } from './portfolio-s3.component';

describe('PortfolioS3Component', () => {
  let component: PortfolioS3Component;
  let fixture: ComponentFixture<PortfolioS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortfolioS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
